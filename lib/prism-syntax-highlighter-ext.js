'use strict'

const prism = require('prismjs')
const loadLanguages = require('prismjs/components/index')

const languages = [
    'abap',
    'abnf',
    'actionscript',
    'ada',
    'agda',
    'al',
    'antlr4',
    'apacheconf',
    'apex',
    'apl',
    'applescript',
    'aql',
    'arduino',
    'arff',
    'armasm',
    'arturo',
    'asciidoc',
    'asm6502',
    'asmatmel',
    'aspnet',
    'autohotkey',
    'autoit',
    'avisynth',
    'awk',
    'bash',
    'basic',
    'batch',
    'bbcode',
    'bbj',
    'bicep',
    'birb',
    'bison',
    'bnf',
    'bqn',
    'brainfuck',
    'brightscript',
    'bro',
    'bsl',
    'c',
    'cfscript',
    'chaiscript',
    'cil',
    'cilkc',
    'cilkcpp',
    'clike',
    'clojure',
    'cmake',
    'cobol',
    'coffeescript',
    'concurnas',
    'cooklang',
    'coq',
    'cpp',
    'crystal',
    'csharp',
    'cshtml',
    'csp',
    'css',
    'csv',
    'cue',
    'cypher',
    'd',
    'dart',
    'dataweave',
    'dax',
    'dhall',
    'diff',
    'django',
    'docker',
    'dot',
    'ebnf',
    'editorconfig',
    'eiffel',
    'ejs',
    'elixir',
    'elm',
    'erb',
    'erlang',
    'etlua',
    'factor',
    'false',
    'flow',
    'fortran',
    'fsharp',
    'ftl',
    'gap',
    'gcode',
    'gdscript',
    'gedcom',
    'gettext',
    'gherkin',
    'git',
    'glsl',
    'gml',
    'gn',
    'go',
    'gradle',
    'graphql',
    'groovy',
    'haml',
    'handlebars',
    'haskell',
    'haxe',
    'hcl',
    'hlsl',
    'hoon',
    'hpkp',
    'hsts',
    'http',
    'ichigojam',
    'icon',
    'idris',
    'iecst',
    'ignore',
    'inform7',
    'ini',
    'io',
    'j',
    'java',
    'javadoc',
    'javadoclike',
    'javascript',
    'javastacktrace',
    'jexl',
    'jolie',
    'jq',
    'jsdoc',
    'json',
    'json5',
    'jsonp',
    'jsstacktrace',
    'jsx',
    'julia',
    'keepalived',
    'keyman',
    'kotlin',
    'kumir',
    'kusto',
    'latex',
    'latte',
    'less',
    'lilypond',
    'liquid',
    'lisp',
    'livescript',
    'llvm',
    'log',
    'lolcode',
    'lua',
    'magma',
    'makefile',
    'markdown',
    'markup',
    'mata',
    'matlab',
    'maxscript',
    'mel',
    'mermaid',
    'metafont',
    'mizar',
    'mongodb',
    'monkey',
    'moonscript',
    'n1ql',
    'n4js',
    'naniscript',
    'nasm',
    'neon',
    'nevod',
    'nginx',
    'nim',
    'nix',
    'nsis',
    'objectivec',
    'ocaml',
    'odin',
    'opencl',
    'openqasm',
    'oz',
    'parigp',
    'parser',
    'pascal',
    'pascaligo',
    'pcaxis',
    'peoplecode',
    'perl',
    'php',
    'phpdoc',
    'plsql',
    'powerquery',
    'powershell',
    'processing',
    'prolog',
    'promql',
    'properties',
    'protobuf',
    'psl',
    'pug',
    'puppet',
    'pure',
    'purebasic',
    'purescript',
    'python',
    'q',
    'qml',
    'qore',
    'qsharp',
    'r',
    'racket',
    'reason',
    'regex',
    'rego',
    'renpy',
    'rescript',
    'rest',
    'rip',
    'roboconf',
    'robotframework',
    'ruby',
    'rust',
    'sas',
    'sass',
    'scala',
    'scheme',
    'scss',
    'smali',
    'smalltalk',
    'smarty',
    'sml',
    'solidity',
    'soy',
    'sparql',
    'sqf',
    'sql',
    'squirrel',
    'stan',
    'stata',
    'stylus',
    'supercollider',
    'swift',
    'systemd',
    'tap',
    'tcl',
    'textile',
    'toml',
    'tremor',
    'tsx',
    'tt2',
    'turtle',
    'twig',
    'typescript',
    'typoscript',
    'unrealscript',
    'uorazor',
    'uri',
    'v',
    'vala',
    'vbnet',
    'velocity',
    'verilog',
    'vhdl',
    'vim',
    'warpscript',
    'wasm',
    'wgsl',
    'wiki',
    'wolfram',
    'wren',
    'xeora',
    'xojo',
    'xquery',
    'yaml',
    'yang',
    'zig',
]

loadLanguages(languages)

const START_RX = /<pre class="prism" style="background-color: #[0-9a-f]*"><code>/
const END = '</code></pre>'


// see for documentation:
// https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/custom/
// https://github.com/asciidoctor/asciidoctor/blob/v2.0.x/lib/asciidoctor/syntax_highlighter/highlightjs.rb
function registerSyntaxHighlighter () {
    const Opal = global.Opal
    const Asciidoctor = Opal.Asciidoctor
    const classDef = Opal.klass(null, Asciidoctor.SyntaxHighlighter.Base, 'PrismSyntaxHighlighter')

    Opal.defn(classDef, '$highlight?', function isHighlight () {
        return true
    })
    Opal.defn(classDef, '$highlight', function highlight (node, source, lang, opts) {
        //console.log(source);


        if(prism.languages[lang]) {
            return prism.highlight(source, prism.languages[lang], lang)//.replace(START_RX, '').slice(0, -END.length)
        } else {
            return prism.highlight(source, prism.languages.cpp, "cpp")//.replace(START_RX, '').slice(0, -END.length)
        }

        //global.highlighter.codeToHtml(source, { lang }).replace(START_RX, '').slice(0, -END.length)
    })

    // this will never work because antora will not call it
    /*Opal.defn(classDef, '$docinfo?', function hasDocInfo (location) {
        return true
    })
    Opal.defn(classDef, '$docinfo', function docinfo (location, doc, opts) {
        console.log("Fuck")
        k
        return "html"
    })*/
    Asciidoctor.SyntaxHighlighter.$register(classDef, 'prism')

}

function registerSyntaxHighlighterPreprocessor () {
    return this.process((doc, reader) => {
        const Opal = global.Opal
        const Asciidoctor = Opal.Asciidoctor
        if (Asciidoctor.SyntaxHighlighter.$for('prism') === Opal.nil) registerSyntaxHighlighter()
        return reader
    })
}

function toProc (fn) {
    return Object.defineProperty(fn, '$$arity', { value: fn.length })
}

module.exports.register = function (registry) {
    if ('groups' in registry) {
        // Antora
        registry.groups.$send('[]=', 'prism_syntax_highlighter', toProc(function () {
            this.preprocessor(registerSyntaxHighlighterPreprocessor)
        }))
    } else {
        // standalone Asciidoctor
        registry.register(function () {
            this.preprocessor(registerSyntaxHighlighterPreprocessor)
        })
    }
    //console.log("exported function call");
}
