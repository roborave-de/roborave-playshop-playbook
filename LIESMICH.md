[RoboRAVE Playshop](https://roborave-de.gitlab.io/roborave-playshop-playbook) wird in Asciidoc verfasst.

* Referenz: [AsciiDoc Language Documentation](https://docs.asciidoctor.org/asciidoc/latest/)

# Voraussetzungen

- [Install Docker Desktop on Windows](https://docs.docker.com/desktop/install/windows-install/)
- [Git for Windows](https://gitforwindows.org/)
  - die aktuellste 64-bit.exe
  - Beim Installationsdialog können alle Voreinstellungen belassen werden
  - Die Anwendung Git Bash starten
  - Mit folgenden Befehlen Deinen Namen und E-Mail Adresse setzen (TODO: ist das nötig, oder wird das bereits bei der Installation gemacht?)
```
git config --global user.name "Dein Name"
git config --global user.email "deine-adresse@mail-provider.org"
```
- [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download/other.html)
  - die aktuelleste Windows x64 (exe) IntelliJ IDEA **Community Edition**
- [IntelliJ AsciiDoc Plugin](https://intellij-asciidoc-plugin.ahus1.de/docs/users-guide/quick-start.html)
- [Gitlab Account](https://gitlab.com/)

# RoboRAVE Playshop Skripte herunterladen

Die RoboRAVE Playshop Skripte bestehen aus zwei Teilprojekten:
- RoboRAVE Playshop Playbook
  - enthält die Konfiguration zum Bauen des Projekts
- RoboRAVE Playshop Courses
  - enthält die eigentlichen Skripte

Beide Projekte sind ineinander verschachtelt. Folge diesen Schritten, um sie richtig herunterzuladen:

- Im Windows Explorer klicke mit der rechten Maustaste auf den Ordner, in dem Du die Projektdateien herunterladen möchtest, um das Kontextmenü zu öffnen
- wähle hier `Git Bash here`, um die Git Kommandozeile in diesem Ordner zu starten
- Lade das Playbook herunter mit diesem Befehl in der Git Bash:
```bash
git clone --branch develop https://gitlab.com/roborave-de/roborave-playshop-playbook.git roborave-playshop
```
- In der Git Bash wechsle in das Projektverzeichnis mit diesem Befehl:
```bash
cd roborave-playshop
```
- Initialisierung für `git flow` (s.u.)
```bash
git flow init -d
```
- Lade nun die Kursskripte herunter und initialisiere dieses Projektverzeichnis ebenfalls:
```bash
git clone --branch develop https://gitlab.com/roborave-de/roborave-playshop-courses.git courses
cd courses
git flow init -d

```

# RoboRAVE Playshop in IntelliJ IDEA bearbeiten

- Starte IntelliJ IDEA

![IntelliJ IDEA](https://intellij-asciidoc-plugin.ahus1.de/docs/users-guide/_images/technical-writing/intellij-first-start.png "startup screen")

- Klicke `Open` und öffne den Ordner `roborave-playshop`

![Open File or Project](doc/images/2023-08-12-212530_521x499_jetbrains-idea-ce.png "open dialog")

- Warte solange, bis das Projekt vollständig geladen ist. Das dauert beim ersten mal eine Weile.
- Nun navigiere links in `Project` durch die Verzeichnisstruktur nach `courses/modules/a-MAZE-ing/pages/motors.adoc` und öffne die Datei

![IntelliJ IDEA AsciiDoctor Projekt](doc/images/2023-08-12-211540_1918x1038_jetbrains-idea-ce.png "editor with preview")

- Oberhalb des Editors/der Vorschau werden 3 Leisten eingeblendet:
  - Der erste führt zu [Working with Antora](https://intellij-asciidoc-plugin.ahus1.de/docs/users-guide/features/advanced/antora.html). Diese Leiste verschwindet leider nicht, wenn man den Link öffnet - daher: "Do not show again".
  - Der zweite empfiehlt "soft-wrap" zu aktivieren - ich empfehle es auch.

  ![Settings](doc/images/2023-08-12-213931_984x744_jetbrains-idea-ce.png "Soft-wrap these files")

  - Der dritte schlägt vor Grammatik und Fehlerkorrektur für Deutsch zu installieren - ein guter Vorschlag.

# Zum Projekt RoboRAVE Playshop beitragen

Hier geht es um das Arbeiten mit Git. Es gibt mehrere Möglichkeiten Git zu bedienen. Z.B. direkt aus IntelliJ heraus. Ich persönlich bevorzuge die offiziellen Tools von Git - `git gui` und  `gitk` - und die Kommandozeile. Ich werde die Konzepte mit diesen Tools erklären, Ihr könnt dann aber auch in IntelliJ mit Git arbeiten, soweit es dort unterstützt wird.

## Workflow

Der Workflow orientiert sich an
[Git flow](https://nvie.com/posts/a-successful-git-branching-model/), bzw.
[GitHub flow](https://docs.github.com/en/get-started/quickstart/github-flow)

Git bietet die Möglichkeit den Quelltext innerhalb eines Projektverzeichnisses (_repository_) verschiedenen Zweigen (_branch_) zu verwalten, die sich von einander abgespaltet (_fork_) und wieder zusammengeführt (_merge_) werden können.

Wir verwalten dieses Projekt in mehreren Branches. Zwei davon sind immer vorhanden:

- `main`: dies ist der letzte Veröffentlichte Stand
- `develop`: dies ist die laufende Arbeit, die in die nächste Veröffentlichung einfließt

Wenn wir Änderungen vornehmen, dann erzeugen wir uns zunächst einen eigenen Branch, der von `develop` abzweigt. Wir können den Namen dieses Branches frei wählen. Jedoch gibt es eine Funktion von Git, `git flow`, die uns den Workflow erleichtert, aber auch Vorgaben zur Namenswahl macht.

Der Ablauf, um neue Inhalte zum Projekt beizutragen ist:

- lokales Repository auf aktuellen Stand bringen
- lokalen Branch anlegen
- Änderungen auf lokalen Branch eintragen (_commit_)
- lokalen Branch auf das zentrale Repository hochladen (_push_)
- den Projektverwalter bitten die Änderungen in den `develop` Branch zu übernehmen (_Merge Request_)
- Versionskonflikte auf Gitlab unter _Merge Requests_ lösen

Den Merge Request können wir zum Review der Änderungen nutzen, bevor wir sie in `develop` übernehmen.


### Lokales Repo Aktualisieren

```bash
git pull
```

### Lokalen Branch Anlegen

In den Kursskripten:

```bash
git flow feature start led-matrix
```
Typische Ausgabe:
```terminal
Switched to a new branch 'feature/led-matrix'

Summary of actions:
- A new branch 'feature/led-matrix' was created, based on 'develop'
- You are now on branch 'feature/led-matrix'

Now, start committing on your feature. When done, use:

     git flow feature finish led-matrix
```

### Änderungen eintragen

Hierzu verwende ich am liebsten `git gui`.

- _Unstaged Changes_: Änderungen, die noch nicht eingetragen wurden
  - Klick auf Dateipfad zeigt Änderungen im Hauptfenster
  - Klick auf Dateisymbol verschiebt nach _Staged Changes_
- _Staged Changes_: Änderungen die dem nächsten Eintrag hinzugefügt werden sollen
  - Klick auf Dateipfad zeigt Änderungen im Hauptfenster
  - Klick auf Dateisymbol verschiebt nach _Unstaged Changes_
- Hauptfenster: Änderungen in der ausgewählten Datei (staged oder unstaged)
- _Commit Message_: Kommentar zum Eintrag, Aufbau:
  - Einzeiler bis max. zum Ende des Fensters (78 Zeichen)
  - Leerzeile
  - optionale ausführliche Beschreibung. Auch hier Zeilenlänge nicht überschreiten (wenn möglich)
- Wenn die gewünschten Änderungen unter _Staged Changes_ aufgelistet sind und der Kommentar erstellt wurde:
  - => _Commit_ zum eintragen.


### Änderungen hochladen

Beim ersten mal (pro Branch):

```bash
git push --set-upstream origin feature/led-matrix
```

Danach:

```bash
git push
```

Typische Ausgabe:

```terminal
Enumerating objects: 22, done.
Counting objects: 100% (22/22), done.
Delta compression using up to 16 threads
Compressing objects: 100% (16/16), done.
Writing objects: 100% (18/18), 2.24 KiB | 1.12 MiB/s, done.
Total 18 (delta 11), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for feature/led-matrix, visit:
remote:   https://gitlab.com/roborave-de/roborave-playshop-playbook/-/merge_requests/new?merge_request%5Bsource_branch%5D=feature%2Fled-matrix
remote:
To gitlab.com:roborave-de/roborave-playshop-playbook.git
 * [new branch]      feature/led-matrix -> feature/led-matrix
branch 'feature/led-matrix' set up to track 'origin/feature/led-matrix'.
```

### Merge Request

[Merge Request auf Gitlab](https://gitlab.com/roborave-de/roborave-playshop-courses/-/merge_requests) öffnen.

Wenn eben auf einen Branch gepusht wurde, wird ein Banner eingeblendet, dass vorschlägt einen Merge Request zu erstellen.

Falls nicht: _New merge request_

Nun den Anweisungen folgen.

- Select Source Branch: Dein Branch (hier im Beispiel: `feature/led-matrix`)
- Target Branch: `develop` (ist vorausgewählt)
- => Compare branches and continue

Eine neue Ansicht öffnet sich.

- Title: sinnvolle Kurzbeschreibung
- Desciption: optionale Beschreibung
- Assignee: @lmoellendorf
- Reviewer: nach Bedarf
- => Create merge request


### Versionskonflikte lösen

Wenn Git Konflikte feststellt, werden diese auf Gitlab im Merge Request dargestellt und können dort gelöst werden.

Wenn ihr Hilfe braucht, gebt bitte Bescheid.

# Lokal das Skript bauen

In IntelliJ IDEA wird eine Voransicht des Dokuments angezeigt, an dem du gerade arbeitest. Meistens reicht das. Aber wenn du das ganze Skript auf deinem lokalen Rechner bauen und anschauen möchtest, dann erfährst du im Folgenden wie das geht.

## Antora Docker Image herunterladen

Die zum Bauen benötigten Anwendungen sind in einem Docker Container vorinstalliert. Somit ist sichergestellt, dass auf unseren Workstations und auf dem Server die gleiche Entwicklungsumgebung verwendet wird und das Ergebnis überall gleich ist.

Um das aktuelle Docker Image herunterzuladen und zu testen, ob das Bauen damit funktioniert führe folgende Schritte aus:

- Öffne die Git Bash wie oben beschrieben im Ordner `roborave-playshop`. Führe dort diesen Befehl aus:
```
docker run -u $(id -u) -v $PWD:/$PWD:Z -w $PWD --rm -t registry.gitlab.com/lmoellendorf/docker-antora:latest --cache-dir=$PWD/.cache/antora antora-playbook.yml
```
Beim ersten mal wird das Docker Image heruntergeladen. Das sieht in etwa so aus:
```
Unable to find image 'registry.gitlab.com/lmoellendorf/docker-antora:latest' locally
latest: Pulling from lmoellendorf/docker-antora
31e352740f53: Already exists
a1e50b3961de: Already exists
fdc5b5e5fbfe: Already exists
55fd4a41ee80: Already exists
23199c5db8ec: Pull complete
f94419368a7f: Pull complete
5b73ae31a2f9: Pull complete
f24bd8548ebd: Pull complete
df74c079cfa3: Pull complete
354242ae62a7: Pull complete
Digest: sha256:991ae02e14c36a4ef8a424c1b52ccc4e9945a6340ba766c88f061313d01eedc5
Status: Downloaded newer image for registry.gitlab.com/lmoellendorf/docker-antora:latest
```
Danach wird der eigentliche Befehl ausgeführt. Die Anwendung `antora` wird mit dem Parameter `antora-playbook.yml` aufgerufen. Die Ausgabe sieht in etwa so aus:
```
[clone] .../gitlab.com/roborave-de/roborave-playshop-courses.git [####################################################]
Site generation complete!
Open file:///home/lars/lb_robotik/playshop/roborave-playshop/build/site/index.html in a browser to view your site.
```
Über den ausgegebenen Link kann das gebaute Dokument im Browser geöffnet werden. Das PDF befindet sich im Unterordner `build\assembler\roborave-playshop\0.1.0\roborave-playshop.pdf`.

